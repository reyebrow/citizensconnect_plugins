<?php

// Define this as a constant for now. Later we can do a lookup on it
// For other provinces.
define("ELECTED_OFFICE", "MLA");


/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'type' => 'target',
  'title' => t("Open North - BC Candidate Lookup"),
  'description' => t('Uses postal code data to lookup your BC Candidate in the Open North Database.'),
  'settings form' => 'open_north_bc_candidate_settings_form',
  'requirements met' => 'open_north_bc_candidate_has_req',
  'alter supporter form callback' => 'open_north_bc_candidate_supporter_form_alter',
  'ajax email callback' => 'open_north_bc_candidate_callback',
  'ajax email validate' => 'open_north_bc_candidate_validate',
  'lookup token_info' => 'open_north_bc_candidate_token_info',
  'lookup token_replace' => 'open_north_bc_candidate_token_replace',
);

/**
 * Returns a structured array defining the fields we need to attach to the supporter.
 *
 * @return
 *  An associative array specifying the fields we wish to add to our
 *  new node type.
 */
function _open_north_bc_candidate_installed_fields() {
  // During installation, the t() function is unavailable, so we use get_t()
  // to store the name of the translation function.
  $t = get_t();

  // Initialize empty fields array
  $fields = array();

  //***********************
  // "Federal MP Data" page fields
  //***********************
  // "EDID Found" fields
  $fields['citcon_email_field_on_edid'] = array(
    'field_name' => 'citcon_email_field_on_edid',
    'cardinality' => 1,
    'type' => 'number_integer',
    'settings' => array(
      'citcon hide field' => TRUE,
    ),
  );

  // "Emailed" fields
  $fields['citcon_email_field_on_to'] = array(
    'field_name' => 'citcon_email_field_on_to',
    'cardinality' => 1,
    'type' => 'text',
    'settings' => array(
      'citcon hide field' => TRUE,
    ),
  );

  return $fields;
}

/**
 * Returns a structured array defining the instances for this content type.
 *
 * This is factored into this function so it can be used in both
 * node_example_install() and node_example_uninstall().
 *
 * @return
 *  An associative array specifying the instances we wish to add to our new
 *  node type.
 */
function _open_north_bc_candidate_installed_instances() {
  $t = get_t();

  // Initialize empty instances array
  $insts = array();


  //***********************
  // "Federal MP Data" page instances
  //***********************
  // "EDID Found" instances
  $insts['citcon_email_field_on_edid'] = array(
    'field_name' => 'citcon_email_field_on_edid',
    'label' => $t('Federal EDID Found'),
    'widget' => array(
      'type' => 'number',
    ),
  );

  // "Emailed" instances
  $insts['citcon_email_field_on_to'] = array(
    'field_name' => 'citcon_email_field_on_to',
    'label' => $t('Federal MP Emailed'),
    'widget' => array(
      'type' => 'text_textfield',
    ),
  );

  return $insts;
}

/**
 * Install hook.
 * This is where we'll add our custom fields to the supporter type.
 */
function open_north_bc_candidate_install_on($supporter_type) {

  // Create all the fields we are adding to our supporter type.
  foreach (_open_north_bc_candidate_installed_fields() as $field) {
    $field['entity_types'] = array('citcon_supporter');
    $field['locked'] = TRUE;    
    field_create_field($field);
  }

  // Create all the instances for our fields.
  foreach (_open_north_bc_candidate_installed_instances() as $instance) {
    $instance['entity_type'] = 'citcon_supporter';
    $instance['bundle'] = $supporter_type;
    field_create_instance($instance);
  }


}

/**
 * Call our own submit handler if we need to.
 */
function open_north_bc_candidate_set_supporter_fields(&$form_state, $candidates, $emailed) {

  // Find out the current fields, and find out if we need to attach fields
  // for the plugin.
  $fields = field_info_instances('citcon_supporter', $form_state['values']['type']);
  $insts_keys = array_keys(_open_north_bc_candidate_installed_instances());

  // If we don't have the fields... install them.
  // watchdog('EXISTS???', '<pre>INST_KEYS---'. print_r($insts_keys,1) . '---fields---'. print_r($fields,1) . '</pre>');
  if (!citcon_array_keys_exists($insts_keys, $fields)) {
    open_north_bc_candidate_install_on($form_state['values']['type']);
  }

  // Set the form_state values for our custom fields.
  $langcode = LANGUAGE_NONE;
  $form_state['values']['citcon_email_field_on_edid'][$langcode][0]['value'] = (!empty($candidates['edid']) ? $candidates['edid'] : '');
  $form_state['values']['citcon_email_field_on_to'][$langcode][0]['value'] = $emailed;

}

/**
 * Here's where we can alter the supporter form
 *
 * @param &$form
 *   Array - The form array, passed by reference so we can edit it.
 *
 * @param &$form_state
 *   Array - The form_state array, passed by reference so we can edit it.
 *
 * @param $values
 *   Array - The saved values for the plugin.
 */
function open_north_bc_candidate_supporter_form_alter(&$form, &$form_state, $values) {
  // Find out the current fields, and find out if we need to remove fields
  // for the plugin.
  $fields = field_info_instances('citcon_supporter', $form['type']['#value']);
  $insts_keys = array_keys(_open_north_bc_candidate_installed_instances());

  // If we have the fields... remove them.
  if (citcon_array_keys_exists($insts_keys, $fields)) {
    // Let's delete our custom fields from the form
    citcon_move_field($form, 'citcon_email_field_on_to');
    citcon_move_field($form, 'citcon_email_field_on_edid');
  }


  // Create a fieldset, and move the Postal Code field there.
  $form['citcon_email_target'] += citcon_move_field($form, $values['postal_code_field']);

  // Set the "Find" button's value here, since we use it a few times and need
  // it to be consistant.
  $submit_value = t('Find');

  // Add ajaxyness to the postal code element.
  // TODO: Ensure we're attaching the ajax to the proper field level
  // ...because we're not here. We're just attching it to the #container
  //$form['citcon_email_target'][$values['postal_code_field']]['#ajax'] = array(
  //  'callback' => 'citcon_email_ajax_callback',
  //  'wrapper' => 'citcon-email-target',
  //  'effect' => 'fade',
  //  'keypress' => TRUE,
  //  'trigger_as' => array('value' => $submit_value),
  //);

  // Then we add a "find" button if we don't already have one. Otherwise
  // we put in a reset button
  $form['citcon_email_target']['find'] = array(
    '#type' => 'button',
    '#value' => $submit_value,
    '#weight' => 20,
    '#limit_validation_errors' => array(
      array($values['postal_code_field'])
    ),
    '#executes_submit_callback' => FALSE,
    '#citcon_plugin_load' => TRUE,
    '#citcon_plugin_type' => 'target',
    '#citcon_plugin_id' => 'open_north_bc_candidate',
    '#submit' => array('citcon_email_ajax_submit'),
    '#validate' => array('citcon_email_ajax_validate'),
    '#ajax' => array(
      'callback' => 'citcon_email_ajax_callback',
      'wrapper' => 'citcon-email-target',
      'effect' => 'fade',
      'keypress' => TRUE,
    ),
  );

  // Add the validation handler to the form's Submit button
  $form['actions']['submit']['#citcon_plugin_load'] = TRUE;
  $form['actions']['submit']['#citcon_plugin_type'] = 'target';
  $form['actions']['submit']['#citcon_plugin_id'] = 'open_north_bc_candidate';
  $form['actions']['submit']['#validate'][] = 'citcon_email_ajax_validate';

  // If we found a result... let's post it.
  $candidate_options = array();

  if (isset($form_state['citcon_email_target']['count']) && $form_state['citcon_email_target']['count'] > 0){

    foreach ($form_state['citcon_email_target']['items'] as $key => $target){
      //Display Messages and Warnings
      if (isset($target['msg'])) {
        $form['citcon_email_target']['msg'][] = array(
          '#type' => 'item',
          '#weight' => -20,
          '#description' => $target['msg'],
        );
      }
      if (isset($target['email']) &&  $target['email'] != "" ) {
        $candidate_options['candidate_' . $key] = htmlentities($target['email']);
      }
    }

   $form['citcon_email_target']['candidates'] = array(
      '#type' => 'checkboxes',
      '#options' => $candidate_options,
      '#weight' => -33,
      '#title' => t('Choose one or more of your candidates'),
      '#ajax' => array(
        'callback' => 'open_north_bc_candidate_select_candidate',
        'wrapper' => 'citcon-email-info',
        'keypress' => TRUE,
      ),
    );

  }


}

/**
 * Here's the callback for when people select and deselect members
 * Note that this function is mostly cosmetic. It only affects what the
 * user sees. The actual setting happens in the validate hook
 *
 * @param $form
 *   Array - The form array, passed by reference so we can edit it.
 *
 * @param $form_state
 *   Array - The form_state array, passed by reference so we can edit it.
 *
 */
function open_north_bc_candidate_select_candidate($form, &$form_state) {

  //Blank out the array
  $to_output = $form['citcon_email_info']['display_to']['#addresses'];

  foreach ($form_state['citcon_email_target']['items'] as $key => $target){

      // get whether or not the checkbox is selected
      $value = isset($form_state['values']['candidates']['candidate_' . $key])? $form_state['values']['candidates']['candidate_' . $key] : 0;

      // If the candidate is selected
      if ( isset($value) && strlen($value) > 2 ){
        //Send a nicely formatted list back to the form to tell people
        // Who they've selected
        if (!in_array($target['email'], $form_state['citcon_email']['to'])) {
          $to_output[] = $target['email'];
        }

      }
      // If the candidate is not selected remove them from the to field
      else {
        foreach ($form_state['citcon_email']['to'] as $key=>$to_item){
          if ($to_item == $target['email'] ){
            unset($to_output[$key]);
          } 
        }
      }

  }

  //update the display_to field
  $form['citcon_email_info']['display_to']['#addresses'] = $to_output;

  return $form['citcon_email_info'];

}


/**
 * Callback for both ajax-enabled button.
 *
 * Selects and returns the fieldset with the names in it.
 */
function open_north_bc_candidate_callback($form, &$form_state) {
  return $form['citcon_email_target'];
}

/**
 * Validation handler for the "Find" button.
 */
function open_north_bc_candidate_validate($form, &$form_state) {
  // Load the campaign node, get the plugin data, then the postal code value.
  
  $campaign_node = node_load($form_state['values']['nid']);
  $plugin_data = citcon_email_get_plugin_data('citcon_email_plugin_target', $campaign_node, 'open_north_bc_candidate');
  $plugin_data['result_msg'] = check_plain(citcon_email_get_plugin_data('citcon_email_plugin_target', $campaign_node, 'result_msg'));
  $postal_code = $form_state['values'][$plugin_data['postal_code_field']];
  $supporter = (object) $form_state['values'];
  $postal_code = field_get_items('citcon_supporter', $supporter, $plugin_data['postal_code_field']);
  $postal_code = $postal_code[0]['value'];

  // If we don't have a postal code... skip over the good stuff.
  if (!empty($postal_code)) {
    // Ready the postal code for URL transfer
    $postal_code = strtoupper($postal_code);
    $postal_code = str_replace(' ', '', $postal_code);

    // Get the mp data
    $candidates = open_north_bc_candidate_get_candidate($postal_code);


    $form_state['citcon_email_target'] = array();

    // If we found something, set all the appropriate values
    if (isset($candidates['items']) && is_array($candidates['items']) && !empty($candidates['items']) ) {
      $counter = 0;
      foreach ($candidates['items'] as $key=>$candidate){ 
        // We got a good result... lets create the data to display...
        // Starting with finding an email addy
        if (empty($candidate['email']) && !empty($candidate['name'])) {
          // no email? Don't send, but alert user.
          $result_msg = token_replace($plugin_data['result_msg'], array('cce-result' => $candidate));

          $form_state['citcon_email_target']['items'][$key]['msg'] = t("We found your candidate, @name.<br />But, we couldn't find their email address. Sorry, about this. We've taken note and are working on it.", array('@name' => $candidate['name']));

          watchdog('CitCon', 'Open north found a candidate, @name. But no email. Using this postal code: @postal', array('@name' => $candidate['name'], '@postal' => $postal_code));
        }
        else if (empty($candidate['email']) && empty($candidate['name'])) {
          // no email or name? Don't send, but alert user.
          $form_state['citcon_email_target']['items'][$key]['msg'] = t("We found an candidate, sort of. We couldn't find their name or email address though.<br />Sorry, about this. We've taken note and are working on it.");

          watchdog('CitCon', 'Open north found an MP. But no email or name. Using this postal code: @postal', array('@postal' => $postal_code));
        }
        else {
          // Then the first and last name if we have that data.
          $email = $candidate['name'].' '.'<'.$candidate['email'].'>';
          // watchdog('CANDIDATE_PROCESS', "<pre>" . print_r($candidate,1) . "</pre>---" . $email);
          $counter++;
          // Set the visible result field

          $form_state['citcon_email_target']['items'][$key]['email'] = $email;

        }
      }

      $form_state['citcon_email_target']['count'] = $counter;
      $form_state['citcon_email_target']['edid'] = $candidates['edid'];

    }
    else if (empty($candidates)) {
      $form_state['citcon_email_target'][0]['msg'] = t('Your postal code could not be found. Please make sure you properly typed in your postal code');

      watchdog('CitCon', 'Fed MP lookup recieved JSON result with error. Using this postal code: @postal', array('@postal' => $postal_code));
    }
    else {
      $form_state['citcon_email_target'][0]['msg'] = t("Sorry, something went wrong and we couldn't manage to find your mp. We've made note of this incident and are looking into it.");

      watchdog('CitCon', 'Fed MP lookup recieved abnormal JSON result. Using this postal code: @postal', array('@postal' => $postal_code));
    }
  }


  // If the checkboxes for candidates are checked then update the 'to' fields appropriately.
  $emailed = "";

  foreach ($form_state['citcon_email_target']['items'] as $key => $target){

    $value = isset($form_state['values']['candidates']['candidate_' . $key])? $form_state['values']['candidates']['candidate_' . $key] : 0;

    if ( isset($value) && strlen($value) > 2 ){
      $emailed .= $emailed == "" ? $target['email'] : ", ". $target['email']; 
      if (!in_array($target['email'], $form_state['citcon_email']['to'])) {
        $form_state['citcon_email']['to'][] = $target['email'];
      }
    }
    // If the candidate is not selected remove them from the to field
    else {
      foreach ($form_state['citcon_email']['to'] as $key=>$to_item){
        if ($to_item == $target['email'] ){
          unset($form_state['citcon_email']['to'][$key]);
        } 
      }
    }
  }

  // If haven't really submitted the form, lets rebuild it.
  if (!$form_state['submitted']) {
    $form_state['rebuild'] = TRUE;
  }
  else {
    // Set the data fields so we can save some of it with the supporter
    if (isset($emailed) && $emailed !=""){
      open_north_bc_candidate_set_supporter_fields($form_state, $form_state['citcon_email_target'], $emailed);
    }
  }


}

/**
 * Lookup function.
 *
 * @param $postal_code
 *   String - The postal code used to lookup the data. Must be all CAPS
 *   and no spaces. Like: M6J2G4
 *
 * @return $mp array containing all the available information
 */
function open_north_bc_candidate_get_candidate($postal_code) {

  // Set the base URL
  $api_url = "http://represent.opennorth.ca";

  // Get the base geo data
  $geo_data = file_get_contents($api_url."/postcodes/".$postal_code);
  $geo_data = json_decode($geo_data, TRUE);

  $candidates = array(
    'edid' => Null,
    'items' => array(),
    );

  // Find the Fed MP boundry info
  foreach ($geo_data['boundaries_centroid'] as $boundary) {
    if ($boundary['boundary_set_name'] == "British Columbia electoral district") {
      // We store the unique identifier of the boundary for later
      $candidates['edid'] = $boundary['external_id'];
    }
  }

  // Find the Province Candidate boundry info


  foreach ($geo_data['candidates_centroid'] as $candidate) {

    if ($candidate['elected_office'] == ELECTED_OFFICE ) {
      
      $candidates['items'][] = $candidate;
    }
  }

  // Setup the error if we got nothing.
  if (empty($candidates)) {
    return FALSE;
  }
  return $candidates;
}

/**
 * Settings form, which will attach to "Email" section.
 */
function open_north_bc_candidate_settings_form($form, $form_state, $defaults) {
  // Get the node object
  $node = $form['#node'];

  // Get the field options
  $options = array(NULL => ' - none - ') + citcon_get_supporter_field_options($form['#node']);

  $form = array();
  $form['info'] = array(
    '#type' => 'item',
    '#description' => t('This will enable Auto-Lookup of BC Candidate data.'),
  );
  $form['postal_code_field'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Postal code field'),
    '#description' => t('Select the Postal Code field from the available supporter type fields.'),
    '#default_value' => isset($defaults['postal_code_field']) ? $defaults['postal_code_field'] : NULL,
  );

  return $form;
}

/**
 * A kind of hook_token_info
 */
function open_north_bc_candidate_token_info() {
  return array(
    'mp_name' => array(
      'name' => t('MP Name'),
      'description' => t("Reliable - The name of the MP. Usually like: Jane Smith"),
    ),
    'mp_first_name' => array(
      'name' => t('MP First Name'),
      'description' => t("Not Reliable - The first name of the MP."),
    ),
    'mp_last_name' => array(
      'name' => t('MP Last Name'),
      'description' => t("Not Reliable - The last name of the MP."),
    ),
    'mp_email' => array(
      'name' => t('MP Email'),
      'description' => t("Not Reliable - The email of the MP. Like: jane.smith@example.com"),
    ),
  );
}

/**
 * A kind of hook_token_replace
 */
function open_north_bc_candidate_token_replace($name, $data) {
  $supported_tokens = open_north_bc_candidate_token_info();

  // If it's not our token... get outta here, quick.
  if (!in_array($name, array_keys($supported_tokens))) {
    return FALSE;
  }

  $key = substr($name, 3);
  return $data[$key];
}

/**
 * Bool function to determine if all the requirements are met and it's ready
 * to go.
 *
 * @param $values
 *   Array - Plugin values to check from.
 *
 * @return TRUE or FALSE.
 */
function open_north_bc_candidate_has_req($values) {
  // if we don't have a handle on a postal code field... do nothing!
  if (isset($values['postal_code_field']) && !empty($values['postal_code_field'])) {
    return TRUE;
  }

  return FALSE;
}
